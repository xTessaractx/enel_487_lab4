
/**
Filename: timer.h
Author: Tessa Herzberger and Kiley LeClair
Class: enel487
Project: lab3
*/

/**
This file contains all of the addresses for the registers, as well as the setupRegs function prototype.
*/

#ifndef TIMER_H
#define TIMER_H

#include "main.h"

/**
		The one_twenty_eight_byte struct consists
		of an array of 32 integers, which are
		each 4 bytes in length.
*/
struct one_twenty_eight_byte
{
	int one_twenty_eight_byte_int_array[32];
};

/**
		The eight_byte struct consists
		of an array of 2 integers, which are
		each 4 bytes in length.
*/
struct eight_byte
{
	int eight_byte_int_array[2];
};


void timer_init(void);
int16_t timer_start(void);
int16_t timer_stop(int16_t start_time);
void timer_shutdown(void);

uint32_t generate_random_32(void);
uint64_t generate_random_64(void);

int16_t null_timer(void);
int16_t add_32_timer(void);
int16_t add_64_timer(void);
int16_t mul_32_timer(void);
int16_t mul_64_timer(void);
int16_t div_32_timer(void);
int16_t div_64_timer(void);
int16_t struct_8_timer(void);
int16_t struct_128_timer(void);
int16_t struct_1024_timer(void);

void print_timed_value(int16_t timer);
void timer_init_with_interrupts(void);
	
#endif

