/**
Filename: main.h
Author: Tessa Herzberger and Kiley LeClair
Class: enel487
Project: lab3
*/
/**This file contains all of the register pointers,
static constant integers, and function prototypes used within the main.c file. */


#ifndef MAIN_H
#define MAIN_H

#include "registers.h"
#include <stdint.h>



//Registers are defined in register.h and register.c
extern volatile uint32_t * regRCC_APB2ENR;	 
extern volatile uint32_t * regGPIOB_ODR;
extern volatile uint32_t * regGPIOB_CRH;
extern volatile uint32_t * regGPIOB_BSRR;
extern volatile uint32_t * regGPIOB_BRR;
  
//Serial
extern volatile uint32_t * regGPIOA_CRL; //DONE
extern volatile uint32_t * regGPIOA_ODR; //DONE
extern volatile uint32_t * regGPIOA_CRH; //DONE
extern volatile uint32_t * regGPIOA_BSRR; //DONE
extern volatile uint32_t * regGPIOA_BRR; //DONE

//USART Stuff
extern volatile uint32_t * regRCC_APB1ENR;
extern volatile uint32_t * regUSART_CR1;
extern volatile uint32_t * regUSART_CR2;
extern volatile uint32_t * regUSART_BRR;
extern volatile uint32_t * regUSART_DR;
extern volatile uint32_t * regUSART_SR;

//TIM2
extern volatile uint32_t * regTIM2_CR1;
extern volatile uint32_t * regTIM2_CR2;
extern volatile uint32_t * regTIM2_ERG;
extern volatile uint32_t * regTIM2_CNT;
extern volatile uint32_t * regTIM2_CCR1;
extern volatile uint32_t * regTIM2_PSC;
extern volatile uint32_t * regTIM2_ARR;
extern volatile uint32_t * regTIM2_CCMR1;
extern volatile uint32_t * regTIM2_CCER;
extern volatile uint32_t * regTIM2_DIER;
extern volatile uint32_t * regNVIC_ISER0;


extern volatile uint32_t * 	regTIM4_CR1;
extern volatile uint32_t * 	regTIM4_CR2;             
extern volatile uint32_t * 	regTIM4_SMCR;   
extern volatile uint32_t * 	regTIM4_CCMR1;     
extern volatile uint32_t * 	regTIM4_CCMR2;   
extern volatile uint32_t * 	regTIM4_CCER;    
extern volatile uint32_t * 	regTIM4_PSC;      
extern volatile uint32_t * 	regTIM4_ARR;      
extern volatile uint32_t * 	regTIM4_CCR1;        
extern volatile uint32_t * 	regTIM4_CCR2;   
extern volatile uint32_t * 	regTIM4_CCR3;  
extern volatile uint32_t * 	regTIM4_CCR4; 
extern volatile uint32_t * 	regTIM4_EGR;
extern volatile uint32_t * 	regGPIOB_CRL;
extern volatile uint32_t *  regTIM4_SR;
extern volatile uint32_t *  regTIM4_DMAR;

static const char * LED = "LED";
static const char * ON = "ON";

static const int TO_UPPER_LOWER_BOUNDARY = 97;
static const int TO_UPPER_UPPER_BOUNDARY = 122;
static volatile uint32_t RESET_LIGHTS = 0x01000000;



void print_timer_all(uint32_t, char[], char [30]);
void determine_user_input (char data_array[50]);
void print_string(char [100]);
char TO_UPPER (char);
int Good_To_Go(int);
void get_status(int);
void on(int);
void off(int);
void print_time(void);
void print_date(void);
void restart(char data_array[50]);
void print_help(void);
void stm32_TimerSetup (void);
void change_duty_pwm (uint32_t);
uint32_t getpwmvalue(void);
void pwm_up (void);
void pwm_down (void);


#endif

