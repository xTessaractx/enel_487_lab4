/**
Filename: main.c
Author: Tessa Herzberger and Kiley LeClair
Class: enel487
Project: lab3
*/

#include "main.h"
#include <stdio.h>
#include "serial_driver_interface.h"
#include <ctype.h>
#include "timer.h"

/**
This is the main function. It initializes the values of many variables, then determines what input the user selected.
Finally, it resets everything.
*/
int main()
{
	//Initialize the Constant values that will be used for the Help Statement, as well as the console string.
	char CONSOLE[30] = "ENEL487_IS_AWESOME > ";

	//Initialize the loop counter, done, i and the data_array
	int i = 0;
	int j = 0;
	int length = 0;
	char data_array[50];
	int done = 0;
	//uint32_t pwm = 20;
	

	
	//Call the setupRegs function, timer_init and serial_open function.
	setupRegs();
	serial_open();
	timer_init();
	stm32_TimerSetup();
	
	//change_duty_pwm (pwm);
	
		//Setup the the LED's on the board
  * regRCC_APB2ENR |= 0x08; // Enable Port B clock
	* regGPIOB_ODR  &= ~0x0000FF00;          /* switch off LEDs                    */
	* regGPIOB_CRH  = 0x33333333; //50MHz  General Purpose output push-pull
	
	restart(data_array);
	
	while(1)
	{
		i = 0;
		//Initialize the data_array values to Null.
		for (i = 0; i < 50; i++)
			data_array[i] = '\0';
		
		//Print the console.
		print_string("\n\r");
		print_string(CONSOLE);
		
		//Ensure that I is set to zero.
		j = 0;
		done = 0;
		
		while (done != 1)
		{
			//Get the user input.
			data_array[j] = getbyte();
			length++;
			
			//Check if the input was a backspace and correct for the backspace.
			if(data_array[j] == 8 || data_array[j] == 127)
			{	
				data_array[j] = '\0';
				sendbyte('\b');
				sendbyte(' ');
				sendbyte('\b');
					if (j != 0)
						j--;
					
					length--;
			}
			
			//Convert all of the values of data_array to uppercase.
			data_array[j] = TO_UPPER (data_array[j]);
			
			//Clear the value in the GPIOA_CRL register	
			* regGPIOA_CRL = 0x44444B44;
			
			sendbyte(data_array[j]);
			
			if (data_array[j] == 0xD)
			{
				sendbyte(0xD);
				done = 1;
			}
			j++;
		}

		//determine what statement was entered by the user.
		determine_user_input(data_array);
		
		//Call the restart finction to do it all again! :)
		restart(data_array);
	}
}

void stm32_TimerSetup (void)
{
	*regRCC_APB2ENR |= 0x8; //Turning on the IOPB

  * regGPIOB_CRL = 0xB0000000;
	
	*regRCC_APB1ENR |= 0x4;	//Enable the clock for TIM4
	
	*regTIM4_CR1  |= 0x00000001; //Turn on the ARPE and CEN bits 

	*regTIM4_EGR |= 0x1; //Turn on the UG bit	
	
	*regTIM4_CCMR1 |= 0x6800;      //Turn on the OCIM 2 and 1 bits amd OC1PE bit for channel 2
	
	*regTIM4_CCER  |= 0x00000010; //Turn on the Capture/Compare Enable bit for channel 2
	
	//counter frequency = 72MHz/(PSC + 1)
	//want counter frequency to be 5kHz
	//Therefore, use PSC = 14399, 14200
	//Offset by 10000, which is why ARR = 10000
	//*regTIM4_PSC = 14399;	
	*regTIM4_PSC = 3699;	
	*regTIM4_ARR = 400; //Set the ARR = 10000, because want higher resolution
	
	*regTIM4_CCR1 = 0;
	*regTIM4_CCR2 = *regTIM4_ARR *.04;
//	*regTIM4_CCR2 = 300;
	*regTIM4_CCR3 = 0;
	*regTIM4_CCR4 = 0;
	
	*regTIM4_CR1  |= 0x81; //Turn on the ARPE and CEN bits 

	*regTIM4_EGR |= 0x1; //Turn on the UG bit
	
//	*regTIM4_SR  &= 0x00000000;
//	*regTIM4_DMAR  = 0;
}


void change_duty_pwm (uint32_t percent_num)
{
			const int MAX = 46;
			const int MIN = 14;
	
		if (percent_num >= MIN && percent_num <= MAX) //currentl in place for range of usable values
			{
			*regTIM4_CCR2 = percent_num;
			*regTIM4_EGR |= 0x1; //Turn on the UG bit
			}
		else if(percent_num <= MAX)
			{
			*regTIM4_CCR2 = MAX;
			*regTIM4_EGR |= 0x1; //Turn on the UG bit
			}
			else if(percent_num >= MIN)
			{
			*regTIM4_CCR2 = MIN;
			*regTIM4_EGR |= 0x1; //Turn on the UG bit
			}
				
}

void pwm_up (void)
{
			uint32_t percent_num = *regTIM4_CCR2 +1;
			change_duty_pwm (percent_num);
				
}


void pwm_down (void)
{
			uint32_t percent_num = *regTIM4_CCR2 -1;
			change_duty_pwm (percent_num);
				
}



