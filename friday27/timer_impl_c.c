/**
Filename: timer_impl_c.c
Author: Tessa Herzberger and Kiley LeClair
Class: enel487
Project: lab3
*/

/**
This file contains all of the addresses for the registers, as well as the setupRegs function prototype.
*/

#include <stdint.h>
#include "timer.h"
#include "serial_driver_interface.h"
#include <cmath>


/** The timer_init function takes no parameters.
		It initializes the appropriate parameters
		needed for the TIM2 to properly function.
		It returns no parameters.
*/
void timer_init(void)
{
	* regRCC_APB1ENR |= 0x000000001; //Enable the clock for TIM2
	* regTIM2_ARR = 0x0000FFFF; //Auto Reload register set to the maximum clock frequency, thus counter is not blocked
	* regTIM2_CR1 |= 0x00000091; //Enable the counter and the DIR register to downcount
	* regTIM2_PSC = 0x00000000;
}

/** The timer_start function takes no parameters.
		It returns the value of the TIM2 Counter register
		as an int16_t, which is used as the start time.
*/
int16_t timer_start(void)
{
	int16_t start_time = 0x0000;
	
	start_time = * regTIM2_CNT;
	
	return start_time;
}

/** The timer_stop function takes no parameters.
		It returns the value of the TIM2 Counter register
		as an int16_t, which is used as the stop time.
*/
int16_t timer_stop(int16_t start_time)
{	
	int16_t stop_time = 0x0000;

	stop_time = (* regTIM2_CNT);

	if (stop_time > start_time)
		stop_time = (0xFFFF - start_time + stop_time) % 0xFFFF;
	else
		stop_time = start_time - stop_time;
	
	return stop_time;
}

/** The timer_shutdown function takes no parameters.
		It resets what was done in the timer_init function.
		It returns no parameters.
*/
void timer_shutdown()
{
	* regRCC_APB1ENR &= ~0x1; //Enable the clock for TIM2
	* regTIM2_CR1 &= ~0x00000011; //Enable the counter and the DIR register to downcount
	* regTIM2_ARR = 0x0000; //Auto Reload register set to the maximum clock frequency, thus counter is not blocked

	return;
}

/** The print_timed_value function takes an int16_t.
		It converts each byte of the int16_t to an ascii
		value, then prints it. It returns no parameters.
*/
void print_timed_value(int16_t timer)
{
	int k = 0;
	int16_t timer_temp = 0x0000;
		
		for (k = 0; k < 4; k++)
		{
			timer_temp = (timer & (0xF000 >> (k * 4))) >> ((3 - k) * 4);			
			
			if (timer_temp < 10)
				timer_temp += 48;
			else if (timer_temp >= 10 && timer_temp < 16)
				timer_temp += 55;

			sendbyte(timer_temp);
			
			* regGPIOA_CRL = 0x44444B44;
		}
		
		return;
}

/** The timer_init_with_interrupts function takes no
		parameters. It initializes the appropriate
		parameters required for the TIM2 timer to
		properly function with interrupts.
		It returns no parameters.
*/
void timer_init_with_interrupts(void)
{
	* regRCC_APB1ENR |= 0x1; //Enable the clock for TIM3
	* regTIM2_CR1 |= 0x00000091; //Enable the counter and the DIR register to downcount
	* regTIM2_PSC = 72000000/(2*2*2*2*2*2*2*2*2*2*2*2*2*2*2*2);
	* regTIM2_ARR = 36000;
	* regTIM2_DIER |= 0x1; //Enable the Update Interrupt
	* regNVIC_ISER0 |= 0x1;
	
	return;
}
