/**
Filename: registers.h
Author: Tessa Herzberger and Kiley LeClair
Class: enel487
Project: lab3
*/

/**
This file contains all of the addresses for the registers, as well as the setupRegs function prototype.
*/

#ifndef REGISTERS_H
#define REGISTERS_H


#define PERIPH_BASE           ((uint32_t)0x40000000)
#define AHBPERIPH_BASE        (PERIPH_BASE + 0x20000)
#define RCC_BASE              (AHBPERIPH_BASE + 0x1000)
#define RCC_APB2ENR           (RCC_BASE + 0x18)
#define APB2PERIPH_BASE       (PERIPH_BASE + 0x10000)
#define GPIOB_BASE            (APB2PERIPH_BASE + 0x0C00)
#define GPIOB_ODR             (GPIOB_BASE + 0x0C)
#define GPIOB_CRH             (GPIOB_BASE + 0x04)
#define GPIOB_BSRR            (GPIOB_BASE  + 0x10)
#define GPIOB_BRR             (GPIOB_BASE  + 0x14)
#define INITIAL_LIGHTS        ((uint32_t)0x00000100)
#define SETUP_LIGHT_LOOP      ((uint32_t)0x01000200)
#define CYLON_RIGHT           ((uint32_t)0x80004000)

//Serial Registers
#define GPIOA_BASE       ((uint32_t)0x40010800)
#define GPIOA_CRL     (GPIOA_BASE)
#define GPIOA_ODR        (GPIOA_BASE + 0x0C)
#define GPIOA_CRH   (GPIOA_BASE + 0x04)
#define GPIOA_BSRR   	   (GPIOA_BASE  + 0x10)
#define GPIOA_BRR       (GPIOA_BASE  + 0x14)

//USART Registers
#define RCC_APB1ENR		 (RCC_BASE + 0x1C)
#define USART_BASE    (0x40004400)
#define USART_CR1     (USART_BASE +0x0C) //USART Enable
#define USART_CR2     	   (USART_BASE +0x10) //USART Clock Enable
#define USART_BRR       (USART_BASE  + 0x08)
#define USART_DR      (USART_BASE + 0x04)
#define USART_SR     	   (USART_BASE)

//TIM2 Registers
#define TIM2_BASE    (0x40000000)
#define TIM2_CR1     (TIM2_BASE + 0x00) 
#define TIM2_DIER    (TIM2_BASE + 0x0C) 
#define TIM2_ERG     (TIM2_BASE + 0x14) 
#define TIM2_CNT     (TIM2_BASE + 0x24)
#define TIM2_CCR1    (TIM2_BASE + 0x34) 
#define TIM2_PSC     (TIM2_BASE + 0x28) 
#define TIM2_ARR     (TIM2_BASE + 0x2C)
#define TIM2_CCMR1	 (TIM2_BASE + 0x18)
#define TIM2_CCER 	 (TIM2_BASE + 0x20)
#define NVIC_BASE 	(0xE000E100)

//TIM4 Registers
#define TIM4_BASE             (0x40000800)
#define TIM4_CR1              (TIM4_BASE + 0x00)
#define TIM4_CR2              (TIM4_BASE + 0x04)
#define TIM4_SMCR             (TIM4_BASE + 0x08)
#define TIM4_CCMR1            (TIM4_BASE + 0x18)
#define TIM4_CCMR2            (TIM4_BASE + 0x1c)
#define TIM4_CCER             (TIM4_BASE + 0x20)
#define TIM4_PSC              (TIM4_BASE + 0x28)
#define TIM4_ARR              (TIM4_BASE + 0x2c)
#define TIM4_CCR1             (TIM4_BASE + 0x34)
#define TIM4_CCR2             (TIM4_BASE + 0x38)
#define TIM4_CCR3             (TIM4_BASE + 0x3c)
#define TIM4_CCR4             (TIM4_BASE + 0x40)
#define TIM4_ERG							(TIM4_BASE + 0x14)
#define GPIOB_CRL							(0x40010C00 + 0x00)
#define TIM4_SR 							(TIM4_BASE + 0x10)
#define TIM4_DMAR							(TIM4_BASE + 0x4C)

void setupRegs(void);

#endif
