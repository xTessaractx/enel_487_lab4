/**
Filename: serial_driver_ifacenterface.h
Author: Tessa Herzberger and Kiley LeClair
Class: enel487
Project: lab3
*/

#ifndef SERIAL_DRIVER_IFACE_H
#define SERIAL_DRIVER_IFACE_H

/** Configure and enable the device. */
void serial_open(void);

/**
Send an 8-bit byte to the serial port, using the configured
bit-rate, # of bits, etc. Returns 0 on success and non-zero on
failure.
@param b the 8-bit quantity to be sent.
@pre must have already called serial_open()
*/
void sendbyte(char data);

/**
Gets an 8-bit character from the serial port, and returns it.
@pre must have already called serial_open()
*/
char getbyte(void);
#endif
