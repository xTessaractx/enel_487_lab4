#include "main.h"


void stm32_TimerSetup (void)
{
	const int MAX_DC = 12;
	const int MIN_DC = 3;
	
	*regRCC_APB2ENR |= 0x8; //Turning on the IOPB
  * regGPIOB_CRL = 0xB0000000;
	*regRCC_APB1ENR |= 0x4;	//Enable the clock for TIM4
	*regTIM4_CR1  |= 0x00000001; //Turn on the ARPE and CEN bits 
	*regTIM4_EGR |= 0x1; //Turn on the UG bit	
	*regTIM4_CCMR1 |= 0x6800;      //Turn on the OCIM 2 and 1 bits amd OC1PE bit for channel 2
	*regTIM4_CCER  |= 0x00000010; //Turn on the Capture/Compare Enable bit for channel 2
	
	//counter frequency = 72MHz/(PSC + 1)
	//want counter frequency to be 5kHz
	//Therefore, use PSC = 14399, 14200
	//Offset by 10000, which is why ARR = 10000
	*regTIM4_PSC = 14399;	
	*regTIM4_ARR = 10000; //Set the ARR = 10000, because want higher resolution
	
	*regTIM4_CCR1 = 0;
	*regTIM4_CCR2 = 0;
	*regTIM4_CCR3 = 0;
	*regTIM4_CCR4 = 0;
	
	*regTIM4_CCR2 = (((MAX_DC - MIN_DC) * (100/100)) + MIN_DC) * 100;
	
	*regTIM4_CR1  |= 0x81; //Turn on the ARPE and CEN bits
	*regTIM4_EGR |= 0x1; //Turn on the UG bit
}
