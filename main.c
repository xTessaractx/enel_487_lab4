/**
Filename: main.c
Author: Tessa Herzberger and Kiley LeClair
Class: enel487
Project: lab3
*/

#include "main.h"
#include <stdio.h>
#include "serial_driver_interface.h"
#include <ctype.h>
#include "timer.h"

/**
This is the main function. It initializes the values of many variables, then determines what input the user selected.
Finally, it resets everything.
*/
int main()
{
	//Initialize the Constant values that will be used for the Help Statement, as well as the console string.
	char CONSOLE[30] = "ENEL487_IS_AWESOME > ";

	//Initialize the loop counter, done, i and the data_array
	int i = 0;
	int j = 0;
	int length = 0;
	char data_array[50];
	int done = 0;
	
	//Call the setupRegs function, timer_init and serial_open function.
	setupRegs();
	serial_open();
	timer_init();
	stm32_TimerSetup();
	
		//Setup the the LED's on the board
  * regRCC_APB2ENR |= 0x08; // Enable Port B clock
	* regGPIOB_ODR  &= ~0x0000FF00;          /* switch off LEDs                    */
	* regGPIOB_CRH  = 0x33333333; //50MHz  General Purpose output push-pull
	
	restart(data_array);
	
	while(1)
	{
		i = 0;
		//Initialize the data_array values to Null.
		for (i = 0; i < 50; i++)
			data_array[i] = '\0';
		
		//Print the console.
		print_string("\n\r");
		print_string(CONSOLE);
		
		//Ensure that I is set to zero.
		j = 0;
		done = 0;
		
		while (done != 1)
		{
			//Get the user input.
			data_array[j] = getbyte();
			length++;
			
			//Check if the input was a backspace and correct for the backspace.
			if(data_array[j] == 8 || data_array[j] == 127)
			{	
				data_array[j] = '\0';
				sendbyte('\b');
				sendbyte(' ');
				sendbyte('\b');
					if (j != 0)
						j--;
					
					length--;
			}
			
			//Convert all of the values of data_array to uppercase.
			data_array[j] = TO_UPPER (data_array[j]);
			
			//Clear the value in the GPIOA_CRL register	
			* regGPIOA_CRL = 0x44444B44;
			
			sendbyte(data_array[j]);
			
			if (data_array[j] == 0xD)
			{
				sendbyte(0xD);
				done = 1;
			}
			j++;
		}

		//determine what statement was entered by the user.
		determine_user_input(data_array);
		
		//Call the restart finction to do it all again! :)
		restart(data_array);
	}
}
