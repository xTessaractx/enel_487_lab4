/**
Filename: arithmetic.c
Author: Tessa Herzberger and Kiley LeClair
Class: enel487
Project: lab3
*/

#include <stdint.h>
#include "timer.h"
//#include "serial_input_output.h"
#include <stdlib.h>

/** The generate_random_32 function takes no parameters,
		generates a random uint32_t, then returns it.
*/
uint32_t generate_random_32(void)
{
	uint32_t random32_value1_1 = (rand() % 32768) << 16;
	uint32_t random32_value1_2 = (rand() % 32768) | random32_value1_1;
	
	return random32_value1_2;
}

/** The generate_random_64 function takes no parameters,
		generates a random uint64_t by generating two
		random uint32_t, then returns the uint64_t.
*/
uint64_t generate_random_64(void)
{
	uint32_t random32_value1 = generate_random_32();
	uint32_t random32_value2 = generate_random_32();

	uint64_t random64_value1 = random32_value1;
	random64_value1 <<= 32;
	random64_value1 += random32_value2;
	
	return random64_value1;
}

/** The null_timer function takes no parameters,
		times how long it takes to call the timer_start
		and timer_stop functions, stores the time into
		an int16_t, then returns it.
*/
int16_t null_timer(void)
{
	int i = 0;
	int16_t time_start_val = 0x0;
	int16_t time_length = 0x0;
	int16_t avg_time = 0x0;
	
	for (i = 0; i < 100; i++)
	{
		//Start timer
		time_start_val = timer_start();

		//end timer
		time_length += (timer_stop(time_start_val));
	}
	
		avg_time = (time_length)/100;
	
	return avg_time;
}

/** The add_32_timer function takes no parameters,
		generates two random 32 bit numbers by calling
		the generate_random_32 function, then times how
		long it takes to perform the mathematic operation.
		The time is stored into an int16_t, then returned.
*/
int16_t add_32_timer(void)
{
	int k = 0;
	int16_t time_start_val = 0;
	int32_t time_length = 0;
	int16_t avg_time = 0;
	int16_t actual_time = 0;
	volatile uint32_t random32_value_1;
	volatile uint32_t random32_value_2;
	volatile uint32_t solution;	
	volatile uint32_t total;	
	
	for(k = 0; k <100; k++)
	{
		random32_value_1 = generate_random_32();
		random32_value_2 = generate_random_32();
					
		//Start timer
		time_start_val = timer_start();
				
		solution = random32_value_1 + random32_value_2;
			
		//end timer
		time_length += (timer_stop(time_start_val));
		
		total += solution;
	}
				
	avg_time = (time_length)/100;
	actual_time = (avg_time) - null_timer();  //- null_timer();
	
	return actual_time;
}

/** The add_64_timer function takes no parameters,
		generates two random 64 bit numbers by calling
		the generate_random_64 function, then times how
		long it takes to perform the mathematic operation.
		The time is stored into an int16_t, then returned.
*/
int16_t add_64_timer(void)
{
	int k = 0;
	int16_t time_start_val = 0;
	int16_t time_length = 0;
	int16_t avg_time = 0;
	int16_t actual_time = 0;
	volatile uint64_t random64_value_1;
	volatile uint64_t random64_value_2;
	volatile uint64_t solution;	
	volatile uint64_t total;	
	
	for(k = 0; k <100; k++)
	{
		random64_value_1 = generate_random_64();
		random64_value_2 = generate_random_64();
					
		//Start timer
		time_start_val = timer_start();
				
		solution = random64_value_1 + random64_value_2;
			
		//end timer
		time_length += (timer_stop(time_start_val));
		
		total += solution;
	}
				
	avg_time = (time_length)/100;
	actual_time = (avg_time)  - null_timer();
	
	return actual_time;
}

/** The mul_32_timer function takes no parameters,
		generates two random 32 bit numbers by calling
		the generate_random_32 function, then times how
		long it takes to perform the mathematic operation.
		The time is stored into an int16_t, then returned.
*/
int16_t mul_32_timer(void)
{
	int k = 0;
	int16_t time_start_val = 0;
	int16_t time_length = 0;
	int16_t avg_time = 0;
	int16_t actual_time = 0;	
	volatile uint32_t random32_value_1;
	volatile uint32_t random32_value_2;
	volatile uint32_t solution;	
	volatile uint64_t total;
				
	for(k = 0; k <100; k++)
	{
		random32_value_1 = generate_random_32();
		random32_value_2 = generate_random_32();
					
		//Start timer
		time_start_val = timer_start();
				
		solution = random32_value_1 * random32_value_2;
			
		//end timer
		time_length += (timer_stop(time_start_val));
		
		total += solution;
	}
				
			
	avg_time = (time_length)/100;
	actual_time = (avg_time)  - null_timer();
	
	return actual_time;
}


/** The mul_64_timer function takes no parameters,
		generates two random 64 bit numbers by calling
		the generate_random_64 function, then times how
		long it takes to perform the mathematic operation.
		The time is stored into an int16_t, then returned.
*/
int16_t mul_64_timer(void)
{
	int k = 0;
	int16_t time_start_val = 0;
	int16_t time_length = 0;
	int16_t avg_time = 0;
	int16_t actual_time = 0;
	volatile uint64_t random64_value_1;
	volatile uint64_t random64_value_2;
	volatile uint32_t solution;	
	volatile uint64_t total;
	
	solution = 0x0000000000000000;
				
	for(k = 0; k <100; k++)
	{
		random64_value_1 = generate_random_64();
		random64_value_2 = generate_random_64();
					
		//Start timer
		time_start_val = timer_start();
				
		solution = random64_value_1 * random64_value_2;
			
		//end timer
		time_length += (timer_stop(time_start_val));
		
		total += solution;
	}
				
			
	avg_time = (time_length)/100;
	actual_time = (avg_time)  - null_timer();
	//print avg time
	
	return actual_time;
}

/** The div_32_timer function takes no parameters,
		generates two random 32 bit numbers by calling
		the generate_random_32 function, then times how
		long it takes to perform the mathematic operation.
		The time is stored into an int16_t, then returned.
*/
int16_t div_32_timer(void)
{
	int k = 0;
	int16_t time_start_val = 0;
	int16_t time_length = 0;
	int16_t avg_time = 0;
	int16_t actual_time = 0;
	volatile uint32_t random32_value_1;
	volatile uint32_t random32_value_2;
	volatile uint32_t solution;	
	volatile uint64_t total;
	
	solution = 0x0000000000000000;
				
	for(k = 0; k <100; k++)
	{
		random32_value_1 = generate_random_32();
		random32_value_2 = generate_random_32();
					
		//Start timer
		time_start_val = timer_start();
		
		if (random32_value_2 != 0x00000000)
			solution = random32_value_1 / random32_value_2;
		else
			solution = 1;
		//end timer
		time_length += (timer_stop(time_start_val));
		
		total += solution;
	}
				
			
	avg_time = (time_length)/100;
	actual_time = (avg_time)  - null_timer();
	//print avg time
	
	return actual_time;
}

/** The div_64_timer function takes no parameters,
		generates two random 64 bit numbers by calling
		the generate_random_64 function, then times how
		long it takes to perform the mathematic operation.
		The time is stored into an int16_t, then returned.
*/
int16_t div_64_timer(void)
{
	int k = 0;
	int16_t time_start_val = 0;
	int16_t time_length = 0;
	int16_t avg_time = 0;
	int16_t actual_time = 0;
	volatile uint64_t random64_value_1;
	volatile uint64_t random64_value_2;
	volatile uint32_t solution;	
	volatile uint64_t total;
	
	solution = 0x0000000000000000;
				
	for(k = 0; k <100; k++)
	{
		random64_value_1 = generate_random_64();
		random64_value_2 = generate_random_64();
					
		//Start timer
		time_start_val = timer_start();
		
		if (random64_value_2 != 0x00000000)
			solution = random64_value_1 / random64_value_2;
		else
			solution = 1;
		//end timer
		time_length += (timer_stop(time_start_val));
		total += solution;
	}
				
			
	avg_time = (time_length)/100;
	actual_time = (avg_time)  - null_timer();
	//print avg time
	
	return actual_time;
}

/** The struct_8_timer function takes no parameters,
		generates a random eight_byte struct using the
		rand() function, then times how long it takes
		to perform copy the struct into another struct.
		The time is stored into an int16_t, then returned.
*/
int16_t struct_8_timer(void)
{
	int k = 0;
	int l = 0;
	int16_t time_start_val = 0;
	int16_t time_length = 0;
	int16_t avg_time = 0;
	int16_t actual_time = 0;
	volatile struct eight_byte eb_1;
	volatile struct eight_byte eb_2;
	volatile uint64_t total;
								
	for(k = 0; k < 2; k++)
	{
		eb_1.eight_byte_int_array[k] = (rand() % 16);
	}
		
	for (k = 0; k < 100; k++)
	{
		time_start_val = timer_start();
		for (l = 0; l < 2; l++)
			eb_2.eight_byte_int_array[l] = eb_1.eight_byte_int_array[l];
		time_length += (timer_stop(time_start_val));
		
		total += eb_2.eight_byte_int_array[l];
	}
		
	avg_time = (time_length)/100;
	actual_time = (avg_time)  - null_timer();
	
	return actual_time;
}

/** The struct_1288_timer function takes no parameters,
		generates a random one_twenty_eight_byte struct using the
		rand() function, then times how long it takes
		to perform copy the struct into another struct.
		The time is stored into an int16_t, then returned.
*/
int16_t struct_128_timer(void)
{
	int k = 0;
	int l = 0;
	int16_t time_start_val = 0;
	int16_t time_length = 0;
	int16_t avg_time = 0;
	int16_t actual_time = 0;
	volatile struct one_twenty_eight_byte eb_1;
	volatile struct one_twenty_eight_byte eb_2;
	volatile uint64_t total;
						
	for(k = 0; k < 32; k++)
		eb_1.one_twenty_eight_byte_int_array[k] = (rand() % 16);
		
	for (k = 0; k < 100; k++)
	{		
		for (l = 0; l < 32; l++)
		{
			time_start_val = timer_start();
			eb_2.one_twenty_eight_byte_int_array[l] = eb_1.one_twenty_eight_byte_int_array[l];
			time_length += (timer_stop(time_start_val));
		}
		
		total += eb_2.one_twenty_eight_byte_int_array[l];
	}
		
	avg_time = (time_length)/100;
	actual_time = (avg_time)  - null_timer();
	
	return actual_time;
}

/** The struct_1024_timer function takes no parameters,
		generates a random eight_byte struct using the rand()
		function, then times how long it takes to perform copy
		the struct into another struct. This is performed until
		1024 bytes are generated using the eight_byte struct.
		The time is stored into an int16_t, then returned.
*/
int16_t struct_1024_timer(void)
{
	int j = 0;
	int k = 0;
	int l = 0;
	
	int16_t time_start_val = 0;
	int16_t time_length = 0;
	int16_t avg_time = 0;
	uint32_t actual_time = 0;
	volatile struct eight_byte eb_1;
	volatile struct eight_byte eb_2;
	volatile uint32_t total;
		
	for (k = 0; k < 100; k++)
	{	
	
		for (j = 0; j < 256; j++)
		{
			for(l = 0; l < 2; l++)
			{
				eb_1.eight_byte_int_array[l] = (rand() % 16);
			}
			
			l = 0;
			
			for (l = 0; l < 2; l++)
			{
				time_start_val = timer_start();	
				eb_2.eight_byte_int_array[l] = eb_1.eight_byte_int_array[l];
				time_length += (timer_stop(time_start_val));
			}
		}
	}
		
	avg_time = (time_length)/100;
	actual_time = (avg_time/200) - null_timer();
	
	return actual_time;
}
